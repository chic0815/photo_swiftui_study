//
//  ContentView.swift
//  Photo SwiftUI
//
//  Created by 이재성 on 2019/11/23.
//  Copyright © 2019 이재성. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    @State var image: Image? = nil
    @State var showCaptureImageView = false
    
    var body: some View {
        ZStack {
            VStack {
                Button(action: {
                    self.showCaptureImageView.toggle()
                }) {
                    Text("Choose Photos")
                }
                
                self.image?.resizable()
                    .frame(width: 250, height: 250, alignment: .center)
            }
            if showCaptureImageView {
                CaptureImageView(isShown: $showCaptureImageView, image: $image)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
